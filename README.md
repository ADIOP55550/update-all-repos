# update-all-repos

A simple python script updating (pull and/or push) all repos found in file ~/repos_to_update. Can be given list of paths (see --help)